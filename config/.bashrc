#!/bin/bash

alias nuget='mono /usr/local/bin/nuget.exe'

function inflate() {
  local file=$1
  local to_dir=$2
  if [ -f "$file" ]; then
    tar --use-compress-program zstd -xf "$file" -C "$to_dir"
    rm "$file"
  fi
}

update_java_home() {
  local java_path
  java_path="$(asdf which java)"
  if [[ -n "${java_path}" ]]; then
    export JAVA_HOME
   JAVA_HOME="$(dirname "$(dirname "$(realpath "${java_path}")")")"
  fi
}

function switch_to() {
  local tool=$1
  local major_version=$2
  local version
  version="$(grep "$tool" "$HOME/.tool-versions"| tr ' ' '\n' | grep "^$major_version")"
  asdf shell "$tool" "$version"

  if [[ "$tool" = "java" ]]; then
    update_java_home
  fi
}

function major_version_from() {
  echo "$1" | cut -d'.' -f1
}

function enable_dev_mode() {
  unset HISTFILESIZE
  unset HISTSIZE
  export EDITOR=vim
  export LOG_LEVEL=debug
  set -o vi
  apt-get update -y
  apt-get install -y --no-install-recommends vim less shellcheck
}

inflate /opt/asdf.tar.zst /opt
inflate /usr/lib/gcc.tar.zst /usr/lib
inflate /usr/lib/mono.tar.zst /usr/lib
inflate /usr/lib/rustlib.tar.zst /usr/lib
inflate /usr/share/dotnet.tar.zst /usr/share

# shellcheck source=/dev/null
. "$ASDF_DATA_DIR/asdf.sh"
# shellcheck source=/dev/null
. "$ASDF_DATA_DIR/completions/asdf.bash"
