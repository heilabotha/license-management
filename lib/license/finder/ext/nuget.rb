# frozen_string_literal: true

module LicenseFinder
  class Nuget
    def prepare_command
      "mono /usr/local/bin/nuget.exe restore -Verbosity detailed"
    end

    def license_urls(dependency)
      filename = "#{dependency.name}.#{dependency.version}.nupkg"
      files = Dir["**/#{filename}"] +
        Dir.glob(File.join(Dir.home, '.nuget', 'packages', '**', '**', filename.downcase))

      return if files.empty?

      Zip::File.open(files.first) do |zipfile|
        Nuget.nuspec_license_urls(zipfile.read(dependency.name + '.nuspec'))
      end
    end
  end
end
