# frozen_string_literal: true

module LicenseFinder
  class Conan
    def possible_package_paths
      [project_path.join('conanfile.txt')]
    end

    def current_packages
      Dir.chdir(project_path) do
        shell.execute([:conan, :install, '.'])
      end
      stdout, _stderr, status = Dir.chdir(project_path) do
        shell.execute([:conan, :info, '-j', '/dev/stdout', '.'])
      end
      return [] unless status.success?

      parse(stdout.lines[0]).map { |dependency| map_from(dependency) }.compact
    end

    private

    def extract_name_version_from(name)
      name.split('@', 2).first.split('/', 2)
    end

    def map_from(dependency)
      name, version = extract_name_version_from(dependency['reference'])
      return if name == 'conanfile.txt'

      Dependency.new('Conan', name, version, spec_licenses: licenses_for(dependency), detection_path: detected_package_path)
    end

    def licenses_for(dependency)
      dependency['license']
    end

    def parse(line)
      JSON.parse(line)
    end
  end
end
