# frozen_string_literal: true

module LicenseFinder
  class Pip
    def current_packages
      return legacy_results unless virtual_env?

      dependencies = python.pip_licenses(detection_path: detected_package_path)
      dependencies.any? ? dependencies : legacy_results
    end

    def possible_package_paths
      path = project_path || Pathname.pwd

      [
        path.join(@requirements_path),
        path.join('setup.py')
      ]
    end

    def prepare
      return install_packages if detected_package_path == @requirements_path

      requirements_path = detected_package_path.dirname.join('requirements.txt')
      requirements_path.write('.') unless requirements_path.exist?
      install_packages
    end

    private

    def python
      @python ||= ::License::Management::Python.new
    end

    def install_packages
      within_project_dir do
        shell.execute(['virtualenv -p', python_executable, '--activators=bash --seeder=app-data .venv'])
        shell.sh([". .venv/bin/activate", "&&", pip_install_command], env: python.default_env)
      end
    end

    def pip_install_command
      [:pip, :install, '-v', '-i', python.pip_index_url, '-r', @requirements_path]
    end

    def python_executable
      '"$(asdf where python)/bin/python"'
    end

    def virtual_env?
      within_project_dir { File.exist?('.venv/bin/activate') }
    end

    def within_project_dir
      Dir.chdir(project_path) { yield }
    end

    def legacy_results
      pip_output.map do |name, version, _children, _location|
        spec = PyPI.definition(name, version)
        Dependency.new(
          'Pip',
          name,
          version,
          description: spec['description'],
          detection_path: detected_package_path,
          homepage: spec['home_page'],
          spec_licenses: PipPackage.license_names_from_spec(spec)
        )
      end
    end
  end
end
