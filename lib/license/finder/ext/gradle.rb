# frozen_string_literal: true

module LicenseFinder
  class Gradle
    def current_packages
      return [] unless download_licenses

      Pathname
        .glob(project_path.join('**', 'dependency-license.xml'))
        .map(&:read)
        .flat_map { |xml_file| parse_from(xml_file) }.uniq
    end

    def package_management_command
      wrapper? ? './gradlew' : 'gradle'
    end

    private

    def download_licenses
      _stdout, _stderr, status = Dir.chdir(project_path) do
        shell.execute([
          @command,
          ENV.fetch('GRADLE_CLI_OPTS', '--exclude-task=test --no-daemon --debug'),
          'downloadLicenses'
        ], env: { 'TERM' => 'noop' })
      end

      status.success?
    end

    def wrapper?
      File.exist?(File.join(project_path, 'gradlew'))
    end

    def xml_parsing_options
      @xml_parsing_options ||= { 'GroupTags' => { 'dependencies' => 'dependency' } }
    end

    def parse_from(xml_file)
      XmlSimple
        .xml_in(xml_file, xml_parsing_options)
        .fetch('dependency', []).map { |hash| map_from(hash) }
    end

    def map_from(hash)
      Dependency.from(GradlePackage.new(hash, include_groups: @include_groups), detected_package_path)
    end
  end
end
