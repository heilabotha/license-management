require 'spec_helper'

RSpec.describe "bundler" do
  include_examples "each report version", "ruby", "bundler"

  context "when the project depends on an older version of ruby specified in a `.ruby-version` file" do
    it 'installs the required ruby and produces a valid report' do
      runner.add_file('.ruby-version', 'ruby-2.4.9')
      runner.add_file('Gemfile') do
        <<~RAW
source 'https://rubygems.org'

gem 'saml-kit'
        RAW
      end

      report = runner.scan
      expect(report).to match_schema
      expect(report[:licenses]).not_to be_empty
      expect(report[:dependencies].map { |x| x[:name] }).to include("saml-kit")
    end
  end

  context "when a project depends on an older version of bundler" do
    it 'produces a valid report' do
      runner.add_file('Gemfile') do
        <<~RAW
source 'https://rubygems.org'

gem 'saml-kit'
        RAW
      end
      runner.add_file('Gemfile.lock') do
        <<~RAW
GEM
  remote: https://rubygems.org/
  specs:
    activemodel (6.0.2.1)
      activesupport (= 6.0.2.1)
    activesupport (6.0.2.1)
      concurrent-ruby (~> 1.0, >= 1.0.2)
      i18n (>= 0.7, < 2)
      minitest (~> 5.1)
      tzinfo (~> 1.1)
      zeitwerk (~> 2.2)
    builder (3.2.4)
    concurrent-ruby (1.1.5)
    i18n (1.7.1)
      concurrent-ruby (~> 1.0)
    mini_portile2 (2.4.0)
    minitest (5.13.0)
    net-hippie (0.2.7)
    nokogiri (1.10.7)
      mini_portile2 (~> 2.4.0)
    saml-kit (1.1.0)
      activemodel (>= 4.2.0)
      net-hippie (~> 0.1)
      xml-kit (>= 0.3.0, < 1.0.0)
    thread_safe (0.3.6)
    tilt (2.0.10)
    tzinfo (1.2.6)
      thread_safe (~> 0.1)
    xml-kit (0.4.0)
      activemodel (>= 4.2.0)
      builder (~> 3.2)
      nokogiri (~> 1.10)
      tilt (>= 1.4.1)
      xmldsig (~> 0.6)
    xmldsig (0.6.6)
      nokogiri (>= 1.6.8, < 2.0.0)
    zeitwerk (2.2.2)

PLATFORMS
  ruby

DEPENDENCIES
  saml-kit

BUNDLED WITH
   1.17.3
        RAW
      end

      report = runner.scan

      expect(report).to match_schema
      expect(report[:licenses]).not_to be_empty
      expect(report.dependency_names).to include("saml-kit")
    end
  end

  context "when a project depends on bundler `~> 2.0`" do
    it 'produces a valid report' do
      runner.add_file('Gemfile') do
        <<~RAW
source 'https://rubygems.org'

gem 'net-hippie'
        RAW
      end
      runner.add_file('Gemfile.lock') do
        <<~RAW
GEM
  remote: https://rubygems.org/
  specs:
    net-hippie (0.3.2)

PLATFORMS
  ruby

DEPENDENCIES
  net-hippie

BUNDLED WITH
   2.1.4
        RAW
      end

      report = runner.scan

      expect(report).to match_schema
      expect(report[:licenses]).not_to be_empty
      expect(report.find('net-hippie')).to eql({
        name: 'net-hippie',
        description: "net/http for hippies. ☮️",
        url: "https://github.com/mokhan/net-hippie/",
        paths: ['.'],
        licenses: ['MIT']
      })
    end
  end

  context "when passing custom options to license finder" do
    it 'forwards the options to license finder' do
      report = runner.scan(env: {
        'LICENSE_FINDER_CLI_OPTS' => "--debug --aggregate-paths=. ruby"
      })

      expect(report).to match_schema
    end
  end
end
